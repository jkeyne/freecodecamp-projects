import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
const Parser = require('expr-eval').Parser;

class App extends React.Component {
	constructor(props) {
		super(props);
		this.DEFAULT_STATE = {
			output: 0,
			buffer: ''
		};
		this.state = this.DEFAULT_STATE;
		this.onClick = this.onClick.bind(this);
		this.doMath = this.doMath.bind(this);
		this.updateOutput = this.updateOutput.bind(this);
	}
	doMath() {
		let buf = this.state.buffer + this.state.output;
		console.log(buf);
		let result = Parser.evaluate(buf, {});
		console.log(result);
		this.setState({
			output: result,
			buffer: this.DEFAULT_STATE.buffer
		});
	}
	updateOutput(digit) {
		console.log('Updating output: ' + digit);
		let ops = ['+', '-', '*', '/'];
		if (digit === null) {
			this.setState(this.DEFAULT_STATE);
		} else if (digit == '.') {
			if (this.state.output.indexOf(digit) < 0) {
				this.setState({
					output: this.state.output + digit
				});
			}
		} else if(digit === '0' && this.state.output == this.DEFAULT_STATE.output) {
			return;
		} else if(ops.indexOf(digit) >= 0) {
			console.log('digit is an operator');
			// if output hasn't been set, then only allow '-'
			if (this.state.output === this.DEFAULT_STATE.output || this.state.output === '-') {
				if (digit === '-') {
					this.setState({
						output: '-'
					});
				// if output has been set, check if last character in buffer is a op
				} else if(ops.indexOf(this.state.buffer[this.state.buffer.length-1]) >= 0) {
					console.log('last input was an operator');
					let buffer = this.state.buffer;
					if (digit === '-') {
						console.log('appending negative');
						buffer += digit;
					} else {
						console.log('replace operator');
						buffer = buffer.slice(0, buffer.length-1) + digit
					}
					this.setState({
						buffer: buffer,
						output: this.DEFAULT_STATE.output
					});
				}
			} else {
				console.log('adding operator');
				this.setState({
					buffer: this.state.buffer + this.state.output + digit,
					output: this.DEFAULT_STATE.output
				});
			}

			/*
			if (this.state.output !== this.DEFAULT_STATE.output && digit !== '-') {
				this.setState({
					buffer: this.state.output + digit,
					output: this.DEFAULT_STATE.output
				});
			} else {
				this.setState({
					buffer: this.state.buffer.slice(0, this.state.buffer.lenth-1) + digit,
					output: this.DEFAULT_STATE.output
				});
			}
			*/
		} else if (this.state.output === this.DEFAULT_STATE.output) {
			this.setState({
				output: digit
			});
		} else {
			this.setState({
				output: this.state.output + digit
			});
		}
	}
	onClick(event) {
		switch (event.target.id) {
			case "zero":
				this.updateOutput('0');
				break;
			case "one":
				this.updateOutput('1');
				break;
			case "two":
				this.updateOutput('2');
				break;
			case "three":
				this.updateOutput('3');
				break;
			case "four":
				this.updateOutput('4');
				break;
			case "five":
				this.updateOutput('5');
				break;
			case "six":
				this.updateOutput('6');
				break;
			case "seven":
				this.updateOutput('7');
				break;
			case "eight":
				this.updateOutput('8');
				break;
			case "nine":
				this.updateOutput('9');
				break;
			case "decimal":
				this.updateOutput('.');
				break;
			case "add":
				this.updateOutput('+');
				break;
			case "subtract":
				this.updateOutput('-');
				break;
			case "multiply":
				this.updateOutput('*');
				break;
			case "divide":
				this.updateOutput('/');
				break;
			case "clear":
				this.updateOutput(null);
				break;
			case "equals":
				this.doMath();
				break;
		}
	}
	render() {
		return (
			<div className='container-fluid text-center'>
				<h1>Calculator</h1>
				<label for="display">Output: </label><input id="display" type="text" value={this.state.output} />
				<div id="inputCluster" className="container-fluid">
					<div className="row top-buffer">
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="divide">/</button>
					</div>
					<div className="row top-buffer">
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="seven">7</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="eight">8</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="nine">9</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="multiply">*</button>
					</div>
					<div className="row top-buffer">
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="four">4</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="five">5</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="six">6</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="subtract">-</button>
					</div>
					<div className="row top-buffer">
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="one">1</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="two">2</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="three">3</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="add">+</button>
					</div>
					<div className="row top-buffer">
						<button onClick={this.onClick} className="btn btn-danger col right-buffer" id="clear">Clear</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="zero">0</button>
						<button onClick={this.onClick} className="btn btn-secondary col right-buffer" id="decimal">.</button>
						<button onClick={this.onClick} className="btn btn-primary col right-buffer" id="equals">=</button>
					</div>
				</div>
			</div>
		);
	}
}

/*
Layout:

display

/ *
7 8 9 -
4 5 6 +
1 2 3 =
c 0 . =

*/

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
