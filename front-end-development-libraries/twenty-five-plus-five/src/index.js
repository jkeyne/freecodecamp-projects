import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { Provider, connect } from 'react-redux';
import { createStore } from 'redux';

// redux action functions

const SET_TIMER = 'SET_TIMER';
const START_TIMER = 'START_TIMER';
const TICK = 'TICK';

// timer: which timer to set (break, session)
// time: the time to set it to (in minutes)
const setTimer = (timer, time) => {
	return {
		type: SET_TIMER,
		timer: timer,
		time: time
	};
};

const startStopTimer = () => {
	return {
		type: START_TIMER
	};
};

const tick = () => {
	return {
		type: TICK
	};
};

class App extends React.Component {
	constructor(props) {
		super(props);
		this.onClick = this.onClick.bind(this);
	}
	// setup interval to update timer every 1 second, and clear after unload
	componentDidMount() {
		this.interval = setInterval(this.props.tick, 1000);
	}
	componentWillUnmount() {
		clearInterval(this.interval);
	}
	onClick(event) {
		switch(event.target.id) {
			case "break-decrement":
				this.props.setTimer('break', -1);
				break;
			case "session-decrement":
				this.props.setTimer('session', -1);
				break;
			case "break-increment":
				this.props.setTimer('break', 1);
				break;
			case "session-increment":
				this.props.setTimer('session', 1);
				break;
			case "start_stop":
				this.props.startStopTimer();
				break;
			case "reset":
				this.props.setTimer('', null);

				// reset beep audio
				document.getElementById('beep').pause();
				document.getElementById('beep').currentTime = 0;
				break;
			default:
				return;
		}
	}
	render() {
		return (
			<div className='text-center'>
				<h1>25+5 Clock</h1>
				<h2 id="break-label">Break Length</h2>
				<p id="break-length">{this.props.break_length}</p>
				<h2 id="session-label">Session Length</h2>
				<p id="session-length">{this.props.session_length}</p>
				<h2 id="timer-label">{this.props.timer}</h2>
				<p id="time-left">{this.props.time_remaining}</p>
				<audio id="beep" src="http://crca.ucsd.edu/~tapel/fppv/2930clicks/2930clicks_long.wav" />
				<div class='row'>
					<button className='col btn btn-info' id="break-increment" onClick={this.onClick}>Increase break</button>
					<button className='col btn btn-info' id="session-increment" onClick={this.onClick}>Increase session</button>
				</div>
				<div class='row'>
					<button className='col btn btn-info' id="break-decrement" onClick={this.onClick}>Decrease break</button>
					<button className='col btn btn-info' id="session-decrement" onClick={this.onClick}>Decrease session</button>
				</div>
				<div class='row'>
					<button className='col btn btn-primary' id="start_stop" onClick={this.onClick}>Start/stop</button>
					<button className='col btn btn-danger' id="reset" onClick={this.onClick}>Reset</button>
				</div>
			</div>
		);
	}
}

// react-redux stuff
function mapStateToProps(state, ownProps) {
	return {
		timer: state.timer,
		time_remaining: state.time_remaining,
		break_length: state.break_length,
		session_length: state.session_length
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setTimer: (timer, time) => dispatch(setTimer(timer, time)),
		startStopTimer: () => dispatch(startStopTimer()),
		tick: () => dispatch(tick())
	};
};
const Container = connect(mapStateToProps, mapDispatchToProps)(App);

// redux stuff
const DEFAULT_STATE = {
	timer: 'Session',
	time_remaining: '25:00',
	running: 'new',
	break_length: 5,
	session_length:25
};

function formatTimeRemaining(minutes) {
	let m = Math.floor(minutes);
	let s = (minutes - m) * 60;
	return m.toString().padStart(2, '0') + ':' + s.toString().padStart(2, '0');
}

const reducer = (state = DEFAULT_STATE, action) => {
	// copy current state into new_state
	// as all cases (except the default) will need to change it

	// allows this function to not modify state
	let new_state = Object.assign({}, state);
	switch(action.type) {
		case SET_TIMER:
			if (action.time === null) {
				// Reset everything
				new_state = DEFAULT_STATE;
			} else {
				// check type of timer to set, and set the correct value
				switch (action.timer) {
					case 'break':
						if (state.break_length + action.time > 0 && state.break_length + action.time <= 60) {
							new_state.break_length += action.time;
						}
						break;
					case 'session':
						if (state.session_length + action.time > 0 && state.session_length + action.time <= 60) {
							new_state.session_length += action.time;
						}
						break;
					default:
						break;
				}
				// if the timer hasn't been started, then adjust displayed time immediately
				if (action.timer === state.timer.toLowerCase() && state.running === 'new') {
					switch (state.timer) {
						case 'Session':
							new_state.time_remaining = formatTimeRemaining(new_state.session_length);
							break;
						case 'Break':
							new_state.time_remaining = formatTimeRemaining(new_state.break_length);
							break;
						default:
							new_state.time_remaining = 'Unknown';
					}
				}
			}
			return new_state;
		case START_TIMER:
			switch (state.running) {
				case 'new':
					// if it hasn't been started already, then update the time remaining
					new_state.running = 'running';
					switch (state.timer) {
						case 'Session':
							new_state.time_remaining = formatTimeRemaining(state.session_length);
							break;
						case 'Break':
							new_state.time_remaining = formatTimeRemaining(state.break_length);
							break;
						default:
							new_state.time_remaining = 'Unknown';
					}
					break;
				case 'running':
					// if running already then pause
					new_state.running = 'paused';
					break;
				case 'paused':
					// if paused then resume without resetting the timer
					new_state.running = 'running';
					break;
				default:
					break;
			}
			return new_state;
		case TICK:
			// should get run every 1000ms by
			// componentDidMount calling setInterval

			// only do anything if already running
			if (state.running === 'running') {
				// convert from time_remaining (MM:SS) to seconds
				let mmss = state.time_remaining.split(':').map(item => Number.parseInt(item));
				let seconds = (mmss[0] * 60 + mmss[1]);

				// if 0 seconds then start next timer
				// if 1 second  then start beep
				// else just subtract 1 seconds and update the display
				if (seconds === 0) {
					// set new timer
					switch (state.timer) {
						case 'Session':
							new_state.time_remaining = formatTimeRemaining(state.break_length);
							new_state.timer = 'Break';
							break;
						case 'Break':
							new_state.time_remaining = formatTimeRemaining(state.session_length);
							new_state.timer = 'Session';
							break;
						default:
							new_state.time_remaining = 'Unknown';
					}
				} else {
					if (seconds === 1) {
						// play beep audio
						// has to be started early to ensure it plays at 00:00
						document.getElementById('beep').currentTime = 0;
						document.getElementById('beep').play();
					}
					// convert seconds to Date to then get it to a string and extract MM:SS
					new_state.time_remaining = new Date((seconds - 1) * 1000).toISOString().substr(14, 5);
				}
			}
			return new_state;
		default:
			return state;
	}
}

const store = createStore(reducer);

// render stuff
ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<Container />
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
