import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

const AUDIO = {
	Q: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/105[kb]lofi-sega-sample-70.wav.mp3', desc: 'lofi-sega-sample-70'},
	W: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/149[kb]lofi-sega-sample-71.wav.mp3', desc: 'lofi-sega-sample-71'},
	E: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/34[kb]lofi-sega-sample-72.wav.mp3', desc: 'lofi-sega-sample-72'},
	A: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/75[kb]lofi-sega-sample-73.wav.mp3', desc: 'lofi-sega-sample-73'},
	S: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/89[kb]lofi-sega-sample-74.wav.mp3', desc: 'lofi-sega-sample-74'},
	D: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/41[kb]lofi-sega-sample-76.wav.mp3', desc: 'lofi-sega-sample-76'},
	Z: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/47[kb]lofi-sega-sample-77.wav.mp3', desc: 'lofi-sega-sample-77'},
	X: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/62[kb]lofi-sega-sample-78.wav.mp3', desc: 'lofi-sega-sample-78'},
	C: {src: 'https://sampleswap.org/samples-ghost/DRUMS%20(FULL%20KITS)/LO%20FI%20and%208%20BIT%20KITS/Lofi%20Sega%20Samples/97[kb]lofi-sega-sample-79.wav.mp3', desc: 'lofi-sega-sample-79'}
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			desc: 'description'
		};
		this.handleKeyPress = this.handleKeyPress.bind(this);
		this.onClick = this.onClick.bind(this);
  }
	componentDidMount() {
		document.addEventListener('keydown', this.handleKeyPress);
	}
	componentWillUnmount() {
		document.removeEventListener('keydown', this.handleKeyPress);
	}
  handleKeyPress(event) {
		switch (event.keyCode) {
			case 81: // q
				document.getElementById('btn-Q').click();
				break;
			case 87: // w
				document.getElementById('btn-W').click();
				break;
			case 69: // e
				document.getElementById('btn-E').click();
			  break;
			case 65: // a
				document.getElementById('btn-A').click();
			  break;
			case 83: // s
				document.getElementById('btn-S').click();
			  break;
			case 68: // d
				document.getElementById('btn-D').click();
			  break;
			case 90: // z
				document.getElementById('btn-Z').click();
			  break;
			case 88: // x
				document.getElementById('btn-X').click();
			  break;
			case 67: // c
				document.getElementById('btn-C').click();
			  break;
			default:
				break;
		}
	}
	onClick(event) {
		let button = event.target.innerText;
		this.setState({
			desc: AUDIO[button].desc
		});
		let audio_elem = event.target.children[0];
		audio_elem.play();
	}
  render() {
    const button = (label) => (
				 <button onClick={this.onClick} className='drum-pad col btn-lg btn-primary' id={'btn-'+label}>
				 	{label}
				 	<audio src={AUDIO[label].src} className='clip' id={label}/>
				 </button>
		);
    return (
      <div className='container-fluid text-center' id='drum-machine'>
        <h1>Drum Machine</h1>
        <div id='display'>
				 <p>{this.state.desc}</p>
        </div>
        <div className='container' id='drum-pad' role='group'>
          <div className='row'>
            {button('Q')}
            {button('W')}
            {button('E')}
          </div>
          <div className='row'>
            {button('A')}
            {button('S')}
            {button('D')}
          </div>
          <div className='row'>
            {button('Z')}
            {button('X')}
            {button('C')}
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
