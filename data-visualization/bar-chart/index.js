import * as d3 from "https://cdn.skypack.dev/d3@7.1.1";

const DATA_URL = 'https://raw.githubusercontent.com/FreeCodeCamp/ProjectReferenceData/master/GDP-data.json'
function get_quarter(month) {
	return Math.ceil(Number.parseInt(month) / 3);
}

function generateTooltip(gdp, date) {
	let q = get_quarter(date.getMonth());
	return `$${Math.floor(gdp)} Billion\n${date.getFullYear()} Q${q}`
}

const tooltip = d3.select('#chart')
	.append('div')
	.attr('id', 'tooltip')
	.style('position', 'absolute')
	.style('visibility', 'hidden')
	.text('Tooltip')

function mouseover(event) {
	tooltip.style('visibility', 'visible');
	tooltip.style('left', event.pageX + 20);
	tooltip.style('top', event.pageY);
	let temp = event.target.dataset.date.split('-');
	let q = get_quarter(temp[1]);
	tooltip.text(`${temp[0]} Q${q}\n$${event.target.dataset.gdp} Billion`);
	tooltip.attr('data-date', (d, i) => event.target.dataset.date)
	tooltip.attr('data-gdp', (d, i) => event.target.dataset.gdp)
}

function mouseout(event) {
	tooltip.style('visibility', 'hidden');
}

d3.json(DATA_URL).then(data => {
	const padding = 50;
	const dates = data.data.map(item => {
		return new Date(item[0]+'T00:00:00Z');
	});
	const GDP = data.data.map(item => {
		return item[1]
	});

	const w = 800;
	const width = (w-padding*2) / data.data.length;
	const gap = 0;
	const h = 500
	
	const xmin = d3.min(dates);
	const xmax = d3.max(dates);
	xmax.setMonth(xmax.getMonth()+3);
	const ymin = 0;
	const ymax = d3.max(GDP);
	
	const xScale = d3.scaleTime()
		.domain([xmin, xmax])
		.range([padding, w-padding]);
	const yScale = d3.scaleLinear()
		.domain([ymin, ymax])
		.range([h-padding, padding])
	const gdpScale = d3.scaleLinear()
		.domain([ymin, ymax])
		.range([padding, h-padding])

	const scaledGDP = GDP.map(item => gdpScale(item));
	
	const svg = d3.select('#chart')
								.append('svg')
								.attr('width', w)
								.attr('height', h)
	
	svg.selectAll('rect')
		.data(scaledGDP)
		.enter()
		.append('rect')
		.attr('x', (d, i) => i*(width+gap)+padding)
		.attr('y', d => h-d)
		.attr('height', d => d-padding)
		.attr('width', width)
		.attr('class', 'bar')
		.attr('data-date', (d, i) => data.data[i][0])
		.attr('data-gdp', (d, i) => data.data[i][1])

	svg.selectAll('rect').on('mouseover', mouseover).on('mouseout', mouseout);
	const xAxis = d3.axisBottom(xScale);
	const yAxis = d3.axisLeft(yScale);

	svg.append("g")
			 .attr("transform", "translate(0," + (h - padding) + ")")
			 .attr('id', 'x-axis')
			 .call(xAxis);
	svg.append('g')
		.attr('transform', `translate(${padding}, 0)`)
		.attr('id', 'y-axis')
		.call(yAxis);
}).catch(err => {
	console.error('Error: ' + err);
})
